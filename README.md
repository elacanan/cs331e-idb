# cs331e-idb
## Introduction
Our web application is called "WonderSpace." The purpose of this project is to identify and track exoplanets, stars, and constellations. We focus on the stars that are visible by a naked eye from Earth and are part of constellations. Predominant majority of such constellations are in our Milky Way Galaxy.
Overall, we collected data on non-binary stars hosting planets that are visible from Earth with the naked eye (the visual magnitude is less than 6.5). Our database currently has data on 122 such stars, 188 planets they host, and 88 constellations. Constellations data present info on all known constellations. Constellations mostly span through multiple galaxies, however the stars we focused on are predominantly (if not all) in Milky Way.
There are many use cases for this web application. One example could be that a user is curious about planets outside of our solar system and would like to learn more information about the stars they orbit and the constellations they belong to. In this case, the user could navigate to the "Planets" tab on the navigation bar and read about a specific planet. After reading about the planet, they could then be directed to information about its host star. Once they learn about the star, they can finally navigate to information about the corresponding constellation. Our model navigation has been designed in a way to allow the user to navigate between planets, stars and constellations in any order they would like.

## Design
The top of each website's page features a navigation bar which links the home/splash, about and model pages. Each model has a page which lists all of the instances for that model. Users can then click on individual instances to access information about a particular planet, star, or constellation.
Our three interrelated models are planets, stars, and constellations. Each planet instance has the following attributes: constellation, host star, planet mass, status, orbital period, semi-major axis, eccentricity, periastron, discovery year, and description. The constellations and host star attributes are linked attributes that direct the user to the corresponding star and constellation for a given planet. Each star instance has the following attributes: constellation, number of planets, star spectral type, star mass, mag v, star radius, and metallicity. Each star instance page also lists the hosted planets.The constellation attribute links to the corresponding constellation for a given star. Finally, each constellation has the following attributes: english name, principal star, location, declination, an image url, hosted planets, and hosted stars. The hosted star and hosted planets attributes link to the corresponding stars and planets instances for a given constellation instance.
In Phase 2, the models depicted in the schema were implemented as tables in a Postgres database. The following UML class diagram depicts the overall database design of the models and the associations between them.


Figure 1: UML class diagram of the models
Each star instance in the database is associated with one or more planet instances. Each constellation instance can have zero or more associated stars because not all constellations in our database have visible stars with planets. Therefore, a constellation instance can be associated with zero or more planets.

# Tools

## Frontend
For frontend development, Bootstrap was used as a CSS framework for creating the navigation bar and card components that organize the information on the about page, as well as the instances for each of our models in Phase 1. We copied the CDN links from bootstrap’s website into the head tag of the HTML web pages we wanted to use bootstrap on, and then we used prebuilt components and grid system to design/organize each page. The backbone of the website was bootstrapped for a nicer presentation and UI. With Bootstrap, the sets of data were presented in the containers to ensure flexibility on the screens of different sizes.
During Phase 2, we implemented React as a GUI framework for the frontend server and React Router to refactor our HTML websites from Phase 1 into a single-page application (SPA). The frontend team built React components for each model (Planets, Stars and Constellations), type of instance, and user-interface design aspects like the navbar and pagination bar.
React-Bootstrap was also used for design components of the website. Each Bootstrap component was rebuilt into a true React component. React-Bootstrap made it easier to implement a fluid, functional, and simple navbar with a brand logo.

## Searching and Sorting
Searching, sorting and ordering was implemented by creating state variables for the search term, sort, and order-by values. This allows each model component to track changes made by the user with the search term, sort, and order-by values and re-render the page with the correct search, sort and/or order-by parameters applied. For each model, users can enter a search term to search for a specific instance by name, which triggers the page to re-render with all instances that contain the search term anywhere within their name/primary key. This was accomplished by pinging the Flask server with the query parameters, which were then processed and used to query the database for instances that satisfy the parameters. The query results were then returned to the React server in order to display the instances with the applied search, sort, and order-by values. Specific query terms had names "query" - a case-sensitive letter combination, triggering a search of the corresponding instance names, "sort_by" - where the search was thendependent on a certain inctance property available from a drop down menu, and "sort_order" which had 2 options of sorting ascending or descending for the "query"-specific instances based on their "sort_by" selection.

## Backend
For phase two, “planet systems” as a model was replaced with a “constellations” model. For Constellation model, application programming interface (API) call was made to a public database of constellations with a RESTful API which returned a json file with all 88 known constellations. For Planets and Stars models, an API call requested the data about all known planets and stars with their properties (about 4000 stars and 5000 planets) as an XML file. Then the resultant data from these API calls were centralized in a python file called API_Stars.py. After the data was extracted it was parsed filtering only non-binary stars in visual magnitude zone. The data were obtained from APIs with the help of requests python library. Constellations json data and Stars and Planets XML data were parsed using json and Element Tree (ET) python libraries.
The constellations, and filtered stars and planets with their properties were collected and outputted as python dictionaries. These dictionaries were then used to populate Postgres database "spacedb" with the help of pre-built Flask - SQLAlchemy models (files models.py and create_db.py).
In models.py, the classes of “Constellation”, "Stars", and "Planets" were added with its corresponding attributes that integrated Flask, Flask-SQLAlchemy, PostgreSQL that prepared our own database called “spacedb”. Another python file called “create_db.py”, imported the functions from models.py, API_Stars.py, and this specific file loaded the constellations.json file, parsed through the constellations data, and populated in the postgres “spacedb” database as a table called “constellations”. This process was similar to how Planets and Stars populated the tables in the "spacedb" database, where API_Stars.py loaded XML and outputted the dictionaries which populated the tables "stars" and "planets".
The file main.py allowed the frontend React service to request and collect the backend data from the database spacedb to be presented in the models and model instances pages.
API services from Gitlab were also employed on the About page to present information regarding each team member's number of commits and issues, as well as the total number of these statistics.

## Previously in Phase 1
For backend we used API-like service, databases, and micro-framework for python - Flask to spawn HTML pages. We have chosen NASA exoplanet database (db) to pull down the data for 3 planet systems and their attributes. The data displayed on the website required research into API’s and multiple data sources. Majority of the astronomical databases currently use TAP (Table Access Protocol) instead of API, which implies using SQL-like language queries in the URLs returning json with data. We hard-coded the names of 3 planets in the query part of the TAP URL. The resulting json was converted into the list of dictionaries with the use of python packages json and requests. Each dictionary contained the information about the planet, its host star and system, the keys starting with "pl_" contained the values for planets attributes, "st_" - star attributes, and "sy_" - system attributes. For convenience and conservation of funds on GCP during Phase 1, this output python data structure (a list of 3 dictionaries) was hard-coded into the main.py and accessed with Flask. With the help of Flask, the backbone of the website was created.

## Ensuring Functionality of Back-End Development
Ten unit tests were created to test each model (i.e Planets, Stars, and Constellations) utilizing Python unittest. There were at least three unit tests created for “Planets” models, which was through a database search query filtered by name and the connection to which constellation in the constellation model; mass, discovery year and then finding that particular planet name; and inserting a fake planet called ‘Jerkyfox’ then shortly deleting to test that this model was functioning correctly. The three unit tests for Constellation was also a database search query filtered by latin name of what came up first, and to return that constellation’s location; latin name and checking its corresponding and connection to principal star; and lastly inserting a tester constellation named “Monica” with its corresponding tester attributes, then searching for its tester principal star named “Olga”, and deleting the tester constellation from the database after. Then the three unit tests for the “Star” model was equivalently also a database search query filtered by host name star and returning the called host name; then filtering at a specified magnitude for magV with a specific metallicity and returning the called associated hostname; and then inserting a tester star named “Ela” with its tester attributes to the Star table in space and deleting it and committing it. Running these tests in Terminal or Git Bash on Windows as a python file, informs us the number of unit tests passed and the time it took to run it as well.

## Hosting
We used the Google App Engine service on GCP to host our web application. In order to use Google Cloud services, we redeemed an educational coupon that Google provides to students and educators. We then created a project in GCP and a new application on the App Engine platform. Using the Google Cloud Shell, we cloned our project onto the Cloud Shell VM and deployed the web application where it is able to run not only on developer’s computers but virtually any user on the internet and have the processing speed of our website load faster. For a better user experience, namecheap service was used to connect GCP url with a more human-readable name:
wonderspace.me
spacewanderer.me

## Diagrams
The diagram of the website and connections between the web pages presenting model instances is shown above, in the design section. https://yuml.me/ resource was used to generate the diagram.

Application Programming Interface (API) Creation
With Flask, we created endpoints from the URL pages from our website. When our website successfully deploys on the Google Cloud Platform, other users from the internet are able to collect data through RESTful API with the published postman collection link posted on our website’s “About” page.
The APIs we created fall into 3 subcategories:


full API for each model - the list of each instance properties jsons
https://backend-dot-crypto-volt-413616.uc.r.appspot.com/planets/api
https://backend-dot-crypto-volt-413616.uc.r.appspot.com/stars/api
https://backend-dot-crypto-volt-413616.uc.r.appspot.com/constellations/api


full json of a single instance
https://backend-dot-crypto-volt-413616.uc.r.appspot.com/api/planets/11%20Com%20b
https://backend-dot-crypto-volt-413616.uc.r.appspot.com/api/stars/11%20Com
https://backend-dot-crypto-volt-413616.uc.r.appspot.com/api/constellations/Pisces


the search results - json contains only the list of names of the corresponding instances. The APIs below represent only specific examples out of many possibilities.
https://backend-dot-crypto-volt-413616.uc.r.appspot.com/planets/?query=tau&sort_by=eccentricity&sort_order=asc
https://backend-dot-crypto-volt-413616.uc.r.appspot.com/stars/?query=Ce&sort_by=hostname&sort_order=asc
https://backend-dot-crypto-volt-413616.uc.r.appspot.com/constellations/?query=Ce&sort_by=latin_name&sort_order=desc

## Postman
Through utilizing the postman API, we created a preliminary design for our APIs and then implemented as described above. To be able to continuously control the integrity of the data and search capability, we used Postman to create 6-9 tests for each request, and requests types were designed as API examples above. Overall we created 20 requests and 128 tests total. The test collection was published, and the link was shared on the About page.
The tests for each group of requests consisted of standard tests such as the response exists and is equal to 200, the response is a json file that is not empty and contains a specific property. Some tests were pertaining to the group, e.g. the APIs for full lists of instances were tested for responses as arrays of a certain length of not less than a certain length, depending on the model type. The number of instances for models Planets and Stars may be changing depending on the new planets being discovered or vice-versa, non-confirmed planets may lose a planet status. For stars, as we currently are focusing on the visible and non-binary stars, we don't expect this number to change, however we may want to change our focus or expand it from visible stars to other types of stars or all stars. But we do not expect to narrow our sampling. So that we test that the number of planets and stars or the current length of the json list is no less then a certain number such as current number of instances. For constellations, the test was checking for the exact length of json list equal to currently known number of constellations, 88, as we do not expect this number to change soon. Another test for the main full APIs was iterating through all of the instances and checking a certain property, e.g. that all planets discovery year is no less than 1900.
For the second group of the APIs, giving the single instance properties, we were checking the number of the properties and them containing a certain name.
The third group, or search APIs, had the highest number of tests coming from 4 requests. We tried 2 text parameters forking into ascending vs descending order of a certain sorting property, such as metallicity or magV for a star.
When a test collection was completed, it was named WonderSpace and published as a public document, with a link available on "About" page:
https://documenter.getpostman.com/view/33692732/2sA3BheaJo



## Resources

Planets and Stars data supplied from: https://github.com/OpenExoplanetCatalogue/oec_gzip/raw/master/systems.xml.gz

Constellations data supplied from: https://www.datastro.eu/api/explore/v2.1/catalog/datasets/88-constellations/records?limit=90


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/elacanan/cs331e-idb.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/elacanan/cs331e-idb/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
