
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 00:27:00 2024

@author: olga
"""

'''
###### OEC - Open Exoplanetary Catalog
##  https://github.com/OpenExoplanetCatalogue/open_exoplanet_catalogue/
## tree structure example
## https://www.openexoplanetcatalogue.com/planet/11%20Com%20b/

This code creates a dictionary of Stars and a dict of Planets for further use,
extracting the information from the above source.

Additionally, the code creates Constellation dictionary from another source:
#### getting constellations json directly from the web ######
# response = requests.get("https://www.datastro.eu/api/explore/v2.1/catalog/datasets/88-constellations/records?limit=90")
# dict = response.json()
# constellations_lst = dict['results']

INSTRUCTIONS:
    0. It takes about a minute to run the file, may be slower. Be patient.
        There are about 4K starts and 5K planets in the xml file.
    
    1. For repetitive runs, use the block that opens xml from a local dir (default version).
    
    2. For the updates, uncomment the Scraping block and comment the block below. 
        Run the file and save a new oec.xml by dumping to stdout and pasting.
    
    3. Check the printout after file run: currently, 122 stars, 188 planets, 88 constellations.
        These stars are non-binary, visible by naked eye (visual magnitude < 6.5), and have planet(s).
        Current code version does not include binary stars (currently, n=29 for visible w/ planets).
        
    4. To print the star dictionary, uncomment the code at the bottom, right before the last 'return' line

'''
import requests, json

# ------------
# load_json
# ------------
def load_json(filename):
    """
    return: a python dict jsn
    filename: a json file
    """
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

def create_stars_dict():

    # ==============  SCRAPING FROM THE WEB  ==========================
    ##copied text from https://github.com/OpenExoplanetCatalogue/open_exoplanet_catalogue/
    import xml.etree.ElementTree as ET, urllib.request, gzip, io
    url = "https://github.com/OpenExoplanetCatalogue/oec_gzip/raw/master/systems.xml.gz"
    # Decompress the gzipped content and Parse the decompressed content as XML
    oec = ET.parse(gzip.GzipFile(fileobj=io.BytesIO(urllib.request.urlopen(url).read())))
    
    #### getting constellations json directly from the web ######
    response = requests.get("https://www.datastro.eu/api/explore/v2.1/catalog/datasets/88-constellations/records?limit=90")
    dict = response.json()
    constellations_lst = dict['results']
    
    # comment out this block below if you uncomment the block above (web scraping)
# =============================================================================
#     import xml.etree.ElementTree as ET  #, urllib.request, gzip, io
#     # gets the xml file saved previously into the ET tree
#     oec = ET.parse("oec.xml")
#     
#     #### reading local copy of constellations json ######
#     dict = load_json("constellations.json")
#     constellations_lst = dict['results']
# =============================================================================
    
    # =============================================================================
    # Creating Stars and Planets data structures for python
    # =============================================================================

    ###### does not add constellations for binary systems. All systems n = 150
    star_systems = {}
    pl_systems = {}
    # names of keys for adding constellations later
    starnames = []
    plnames = []
    # selects only stars that host planets except binary
    for star in oec.findall(".//star[planet]"):
        #print(star[0].text, len(star.findall(".//planet")))
        magV = star.findtext("magV")
        if magV:
            if float(magV) <= 6.5:
        
                # adding star to the star_systems if it's visible by naked eye
                # star_dict holds properties of a given star
                star_dict = {}
                starname = star.findtext("name")
                starnames.append(starname)
                star_dict['magV'] = magV
                star_dict['spectraltype'] = star.findtext("spectraltype")
                star_dict['mass'] = star.findtext("mass")
                star_dict['radius'] = star.findtext("radius")
                star_dict['metallicity'] = star.findtext("metallicity")
                star_dict['num_planets'] = len(star.findall(".//planet"))
                star_systems[starname] = star_dict
                
                # finding planets for each star
                for planet in star.findall(".//planet"):
                    #print(planet.findtext("name"))
                    plname = planet.findtext("name")
                    plnames.append(plname)
                    planet_dict = {}
                    planet_dict['starname'] = starname
                    planet_dict['status'] = planet.findtext("list")
                    planet_dict['mass'] = planet.findtext("mass")
                    planet_dict['period'] = planet.findtext("period")
                    planet_dict['semimajoraxis'] = planet.findtext("semimajoraxis")
                    planet_dict['eccentricity'] = planet.findtext("eccentricity")
                    planet_dict['periastron'] = planet.findtext("periastron")
                    planet_dict['discoveryyear'] = planet.findtext("discoveryyear")
                    planet_dict['description'] = planet.findtext("description")               
                    pl_systems[plname] = planet_dict
                 
    
    #######################################################################
    # adding constellations as star properties and detecting binary planets
    #######################################################################
    #binary_pl = []
    binary_st = []
    # iterating through the names of stars that are in the star dict
    for starname in starnames:
        # selects only systems with stars that have planets
        # does not find constellation if there's a binary
        for sy in oec.findall(".//star[planet]..."):
            # find star name under the current sy system
            for star in sy.findall("./star[planet]"):
                s = star.findtext("name")
                if starname == s:
                    #print("after if", sy[0].text, len(sy.findall(".//planet")), "constellation:", sy[-1].text)
                    # if can't find constellation name, it means the star is wrapped
                    # in binary tags, and we remove such stars for trimming down
                    if sy.findtext("constellation"):
                        star_systems[starname]['constellation'] = sy.findtext("constellation")
                    else:
                        # finding planets that belong to this star and del the star
                        binary_st.append(starname)
                        del star_systems[starname]
                        
    #######################################################################
    # removing inconsistensies between constellation names in XML and json
    # renaming 'Boötes' into 'Bootes' and fixing typos
    #######################################################################
    starnames = star_systems.keys()
    for s in starnames:
        if star_systems[s]['constellation'] == 'Boötes':
            star_systems[s]['constellation'] = 'Bootes'
        if star_systems[s]['constellation'] == 'Ophiucus':
            star_systems[s]['constellation'] = 'Ophiuchus'
    
    
    
    for planet in plnames:
        if pl_systems[planet]['starname'] in binary_st:
            del pl_systems[planet]
    
    
    # adding constellation to planets dictionary
    # try-except is because after deleting binary planets from pl_systems, we have
    # not deleted these names from pl_names. 
    for p in plnames:
        for s in star_systems:
            try:
                if s == pl_systems[p]['starname']:
                    pl_systems[p]['constellation'] = star_systems[s]['constellation']
            except:
                pass
    
    print("Total number of visible non-binary stars hosting planets:", len(star_systems))
    print("Total number of visible non-binary planets:", len(pl_systems))
    
    #with open("stars_model.json", "w") as outfile:
        #json.dump(star_systems, outfile)
             
    #for k, val in star_systems.items():
    #   print(k, val)
    
    # =============================================================================
    # Creating Constellations data structure for python
    # =============================================================================
    constellation_systems = {}
    for d in constellations_lst:
        k = d['latin_name_nom_latin']
        constellation_systems[k] = {}
        constellation_systems[k]['eng_name'] = d['english_name_nom_en_anglais']
        constellation_systems[k]['principal_star'] = d['principal_star_etoile_principale']
        constellation_systems[k]['location'] = d['quad_repere_de_l_hemisphere_et_du_quadrant']
        constellation_systems[k]['declination'] = d['dec_declinaison']
        constellation_systems[k]['image_filename'] = d['image']['filename']
        constellation_systems[k]['image_url'] = d['image']['url']    
    
    ############# adding number of stars and planets to constellations model ###
    ## updating starnames and plnames
    starnames = []
    plnames = []
    for s in star_systems:
        starnames.append(s)
    for p in pl_systems:
        plnames.append(p)   
    
    for c in constellation_systems:
        n = 0
        k = 0
        for s in starnames:
            if star_systems[s]['constellation']==c:
                n += 1
        constellation_systems[c]['star_num'] = n
        for p in plnames:           
            if pl_systems[p]['constellation']==c:
                k += 1
        constellation_systems[c]['pl_num'] = k
         
        
    print('Total number of constellations', len(constellation_systems))
    
# =============================================================================
#     # checking star num per constellation by 2 methods
#     for c in constellation_systems:
#         print(c, constellation_systems[c]['star_num'])
#     #print(constellation_systems['Indus'])
#         
#     constel = {}   
#     for k, val in star_systems.items():
#         if val["constellation"] not in constel:
#             constel[val["constellation"]] = 1
#         else:
#             constel[val["constellation"]] += 1
# 
#     print(constel)
# =============================================================================
    
    return star_systems, pl_systems, constellation_systems #, starnames, plnames


#star_systems, pl_systems, constellation_systems = create_stars_dict()    
#constellation_systems['Boötes'] = constellation_systems.pop('Bootes')