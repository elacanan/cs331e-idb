# -*- coding: utf-8 -*-
"""
Created on Sat Mar  9 09:20:55 2024

@author: olga
"""
import requests, json

# ------------
# load_json
# ------------
def load_json(filename):
    """
    return: a python dict jsn
    filename: a json file
    """
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

def jprint(obj):
    # create a formatted string of the Python JSON object
    text = json.dumps(obj, sort_keys=True, indent=4)
    print(text)

#### reading local copy of constellations json ######
dict = load_json("constellations.json")
constellations_lst = dict['results']

# =============================================================================
# #### getting constellations json directly from the web ######
# response = requests.get("https://www.datastro.eu/api/explore/v2.1/catalog/datasets/88-constellations/records?limit=90")
# dict = response.json()
# constellations_lst = dict['results']
# =============================================================================

constellation_systems = {}
for d in constellations_lst:
    k = d['latin_name_nom_latin']
    constellation_systems[k] = {}
    constellation_systems[k]['eng_name'] = d['english_name_nom_en_anglais']
    constellation_systems[k]['principal_star'] = d['principal_star_etoile_principale']
    constellation_systems[k]['location'] = d['quad_repere_de_l_hemisphere_et_du_quadrant']
    constellation_systems[k]['declination'] = d['dec_declinaison']
    constellation_systems[k]['image_filename'] = d['image']['filename']
    constellation_systems[k]['image_url'] = d['image']['url']
    
print(len(constellation_systems))
print(constellation_systems['Indus'])    
    

#print(response.status_code)
#print(len(response.json()))
#jprint(response.json())

with open("stars_model.json") as stars:
     jsn = json.load(stars)
     stars.close()
     

# counting # of constellations 
with open("stars_model.json") as user_file:
    stars = json.load(user_file)

constellations = {}   
for k, val in stars.items():
    if val["constellation"] not in constellations:
        constellations[val["constellation"]] = 1
    else:
        constellations[val["constellation"]] += 1

print(constellations)

