#!/usr/bin/env python3

# ---------------------------
# projects/IDB3/create_db.py
# Fares Fraij
# ---------------------------

import json
from models import app, db, Planet, Star, Constellation

from API_stars import create_stars_dict

# ------------
# load_json
# ------------
def load_json(filename):
    """
    return: a python dict jsn
    filename: a json file
    """
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

# ------------
# create_stars
# ------------
def create_stars():
    
    """
    obtaining stars and planets dictionaries from which the tables are populated
    """

    stard, planetd, consteld = create_stars_dict()
    print(len(stard), len(planetd), len(consteld))
      
    """
    populate constellations table
    """
    
    for key, val in consteld.items():
        #print(key)
        latin_name = key
        eng_name = val["eng_name"]
        principal_star = val["principal_star"]
        location = val["location"]
        declination = val["declination"]
        star_num = val["star_num"]
        pl_num = val["pl_num"]
        image_filename = val["image_filename"]
        image_url = val["image_url"]

        newConstel = Constellation(latin_name=latin_name, eng_name=eng_name, 
                       principal_star=principal_star, 
                       location=location, declination=declination,  
                       star_num = star_num, pl_num = pl_num,
                       image_filename = image_filename,
                       image_url = image_url)
                
        # After I create the Star, I can then add it to my session. 
        db.session.add(newConstel)
        # commit the session to my DB - spacedb.
        db.session.commit()


    """
    populate stars table
    """
    
    for key, val in stard.items():
        hostname = key
        spectraltype = val["spectraltype"]
        magV = val["magV"]
        mass = val["mass"]
        radius = val["radius"]
        metallicity = val["metallicity"]
        num_planets = val["num_planets"]
        constellation = val["constellation"]
		
        newStar = Star(hostname=hostname, spectraltype=spectraltype, magV=magV, 
                       mass=mass, radius=radius, metallicity=metallicity, 
                       num_planets=num_planets, constellation=constellation)
                
        # After I create the Star, I can then add it to my session. 
        db.session.add(newStar)
        # commit the session to my DB - spacedb.
        db.session.commit()


    """
    populate planets table
    """
    
    for key, val in planetd.items():
        plname = key
        hostname = val["starname"]
        status = val["status"]
        mass = val["mass"]
        period = val["period"]
        semimajoraxis = val["semimajoraxis"]
        eccentricity = val["eccentricity"]
        periastron = val["periastron"]
        discoveryyear = val["discoveryyear"]
        description = val["description"]
        constellation = val["constellation"]
    
        newPlanet = Planet(plname=plname, hostname=hostname, status=status, 
                       mass=mass, period=period, semimajoraxis=semimajoraxis, 
                       eccentricity=eccentricity, periastron=periastron,
                       discoveryyear=discoveryyear, constellation=constellation, 
                       description=description)
                
        # After I create the Planet, I can then add it to my session. 
        db.session.add(newPlanet)
        # commit the session to my DB - spacedb.
        db.session.commit()        

        
db.drop_all()
db.create_all()
create_stars()

# ------------
# create_books
# ------------
# =============================================================================
# def create_books():
#     """
#     populate book table
#     """
#     book = load_json('books.json')
# 
#     for oneBook in book['Books']:
#         title = oneBook['title']
#         id = oneBook['id']
# 		
#         newBook = Book(title = title, id = id)
#         
#         # After I create the book, I can then add it to my session. 
#         db.session.add(newBook)
#         # commit the session to my DB.
#         db.session.commit()
# 	
# create_books()
# =============================================================================
