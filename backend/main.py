from flask import Flask, render_template, jsonify, request
import requests, json
from create_db import app, db, Planet, Star, Constellation, create_stars
from statistics_gitlab import count_commits
from flask_cors import CORS


CORS(app)

def parse_planets(planets):
    list_planets = []
    for p in planets:
        list_planets.append(p.plname)
    return list_planets

def parse_stars(stars):
    list_stars = []
    for s in stars:
        list_stars.append(s.hostname)
    return list_stars

#####################

# ------------
# Stars
# ------------	

# function takes in a star name (PK = hostname) and outputs a dictionary 
# with the star properties. The func is used inside the decorators below
def s_dict(hostname):
    star = Star.query.filter_by(hostname=hostname).first()
    if star:
        planets = [{'plname': planet.plname} for planet in star.planets]
        
        star_details = {
            'hostname': star.hostname,
            'spectraltype': star.spectraltype,
            'metallicity': star.metallicity,
            'magV': star.magV,
            'mass': star.mass,
            'radius': star.radius,
            'num_planets': star.num_planets,
            'constellation': star.constellation,
            'planets': planets
        }
        return star_details
    else:
        return {'error': 'Star not found'}
        


@app.route('/stars')
@app.route('/stars/')
def get_stars():
    search_query = request.args.get('query', '') # para, default
    sort_by = request.args.get('sort_by', 'mass') 
    sort_order = request.args.get('sort_order', 'asc') 

    query = Star.query

    if search_query:
        query = query.filter(Star.hostname.like(f'%{search_query}%'))

    if sort_order == 'desc':
        query = query.order_by(db.desc(getattr(Star, sort_by)))
    else:
        query = query.order_by(getattr(Star, sort_by))


    # Execute query return sorted list
    st_list = query.all()    
    st_list_json = []
    for obj in st_list:
        star = {}
        star['hostname'] = obj.hostname

        st_list_json.append(star)
    return jsonify(st_list_json)


# displays the star properties page by React, port 3000
@app.route('/api/stars/<hostname>')
def st_properties(hostname):
    star_details = s_dict(hostname)
    return jsonify(star_details)
    
# outputs a json with all stars data, port 8080
@app.route('/stars/api')
def get_starsapi():   
    # Executing query returns a list of all star objects
    s_list = Star.query.all()
    # Creating a list of star dictionaries    
    s_list_json = []
    for obj in s_list:
        star = s_dict(obj.hostname)
        s_list_json.append(star)
    return jsonify(s_list_json)


#####################

# ------------
# Planets
# ------------	

# function takes in a planet name (PK = plname) and outputs a dictionary 
# with the planet properties. The func is used inside the decorators below
def p_dict(plname):
    planet = Planet.query.filter_by(plname=plname).first()
    if planet:
        planet_details = {
            'plname': planet.plname,
            'hostname': planet.hostname,
            'status': planet.status,
            'mass': planet.mass,
            'period': planet.period,
            'semimajoraxis': planet.semimajoraxis,
            'eccentricity': planet.eccentricity,
            'periastron': planet.periastron,
            'discoveryyear': planet.discoveryyear,
            'description': planet.description,
            'constellation': planet.constellation
        }
        return planet_details
    else:
        return {'error': 'Planet not found'}


@app.route('/planets/')
@app.route('/planets')
def get_planets():
    search_query = request.args.get('query', '') # para, default
    sort_by = request.args.get('sort_by', 'mass') 
    sort_order = request.args.get('sort_order', 'asc') 

    query = Planet.query

    if search_query:
        query = query.filter(Planet.plname.like(f'%{search_query}%'))

    if sort_order == 'desc':
        query = query.order_by(db.desc(getattr(Planet, sort_by)))
    else:
        query = query.order_by(getattr(Planet, sort_by))


    # Execute query return sorted list
    pl_list = query.all()    
    pl_list_json = []
    for obj in pl_list:
        planet = {}
        planet['plname'] = obj.plname

        pl_list_json.append(planet)
    return jsonify(pl_list_json)


# displays the planet properties page by React, port 3000
@app.route('/api/planets/<plname>')
def pl_properties(plname):
    planet_details = p_dict(plname)
    return jsonify(planet_details)

    
# outputs a json with all stars data, port 8080
@app.route('/planets/api')
def get_planetsapi():   
    # Executing query returns a list of all planet objects
    p_list = Planet.query.all()
    # Creating a list of star dictionaries    
    p_list_json = []
    for obj in p_list:
        planet = p_dict(obj.plname)
        p_list_json.append(planet)
    return jsonify(p_list_json)


######################

# ------------
# Constellations
# ------------	

# function takes in a constellation name (PK) and outputs a dictionary 
# with the constellation properties. It's used inside 2 decorators below
def c_dict(latin_name):
    constellation = Constellation.query.filter_by(latin_name=latin_name).first()
    if constellation:
        planets = [{'plname': planet.plname} for planet in constellation.planets]
        stars = [{'hostname': star.hostname} for star in constellation.stars]

        constellation_details = {
            'latin_name': constellation.latin_name,
            'eng_name': constellation.eng_name,
            'principal_star': constellation.principal_star,
            'location': constellation.location,
            'declination': constellation.declination,
            'image_filename': constellation.image_filename,
            'image_url': constellation.image_url,
            'planets': planets,  
            'stars': stars
        }
        return constellation_details
    else:
        return {'error': 'Constellation not found'}


@app.route('/constellations')
@app.route('/constellations/')
def get_constellations():
    search_query = request.args.get('query', '') # para, default
    sort_by = request.args.get('sort_by', 'declination') 
    sort_order = request.args.get('sort_order', 'asc') 

    query = Constellation.query

    if search_query:
        query = query.filter(Constellation.latin_name.like(f'%{search_query}%'))

    if sort_order == 'desc':
        query = query.order_by(db.desc(getattr(Constellation, sort_by)))
    else:
        query = query.order_by(getattr(Constellation, sort_by))

    # Execute query return sorted list
    c_list = query.all()    
    c_list_json = []
    for obj in c_list:
        constellation = {}
        constellation['latin_name'] = obj.latin_name

        c_list_json.append(constellation)
    return jsonify(c_list_json)

# displays the constellation properties page by React, port 3000
@app.route('/api/constellations/<latin_name>')
def c_properties(latin_name):
    constellation_details = c_dict(latin_name)
    return jsonify(constellation_details)


# outputs a json with all constellations data, port 8080
@app.route('/constellations/api')
def get_constellationsapi():   
    # Executing query returns a list of all constellation objects
    c_list = Constellation.query.all()
    # Creating a list of constellation dictionaries    
    c_list_json = []
    for obj in c_list:
        constellation = c_dict(obj.latin_name)
        c_list_json.append(constellation)
    return jsonify(c_list_json)


#################
@app.route('/about', methods=['GET'])
def about():
    response = count_commits()
    return response

if __name__ == "__main__":
	app.run(host = '0.0.0.0', port=8080)
