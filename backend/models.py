#!/usr/bin/env python3

# ---------------------------
# projects/IDB3/models.py
# Olga Ostrovskaya, modified from Fares Fraij
# ---------------------------

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

# initializing Flask app 
app = Flask(__name__) 

app.app_context().push()

# Change this accordingly 
USER ="postgres"
PASSWORD ="1234Postgres"
PUBLIC_IP_ADDRESS ="34.66.157.247:5432"
DBNAME ="spacedb"



# Configuration 
app.config['SQLALCHEMY_DATABASE_URI'] = \
os.environ.get("DB_STRING",f'postgresql://{USER}:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True  # To suppress a warning message
db = SQLAlchemy(app)


# ------------
# Constellation
# ------------
class Constellation(db.Model):
    """
    Constellation class has 7 proprietary and 2 image attrbiutes read from xml,
    and 2 derived attributes (from other classes/python dicts)
    
    """
    __tablename__ = 'constellations'
    
    latin_name = db.Column(db.String(80), nullable = False, primary_key = True)
    eng_name = db.Column(db.String(80))
    principal_star = db.Column(db.String(80))
    location = db.Column(db.String(80))
    declination = db.Column(db.String(80))
    image_filename = db.Column(db.String(1000))
    image_url = db.Column(db.String(2000))
    star_num = db.Column(db.Integer)
    pl_num = db.Column(db.Integer)
    planets = db.relationship('Planet', backref = 'constellations')
    stars = db.relationship('Star', backref = 'constellations')


# ------------
# Planet
# ------------
class Planet(db.Model):
    """
    Planet class has 10 attrbiutes read from xml, including description and status
    and 1 attr imported from Star dict (constellations)
    
    """
    __tablename__ = 'planets'
    
    plname = db.Column(db.String(80), nullable = False, primary_key = True)
    hostname = db.Column(db.String(80), db.ForeignKey('stars.hostname'))
    status = db.Column(db.String(80)) # Confirmed or Controversial
    mass = db.Column(db.String(10))        # Jupiter
    period = db.Column(db.Float)     
    semimajoraxis = db.Column(db.Float)
    eccentricity = db.Column(db.String(10))
    periastron = db.Column(db.Float)
    discoveryyear = db.Column(db.Integer)
    constellation = db.Column(db.String(80), db.ForeignKey('constellations.latin_name'))
    description = db.Column(db.String(1000))


# ------------
# Star
# ------------
class Star(db.Model):
    """
    Star class has 9 attrbiutes read from xml  
    
    """
    __tablename__ = 'stars'
    
    hostname = db.Column(db.String(80), nullable = False, primary_key = True)
    magV = db.Column(db.Float, nullable = False)
    spectraltype = db.Column(db.String(80))
    mass = db.Column(db.Float)
    radius = db.Column(db.Float)
    metallicity = db.Column(db.Float)
    num_planets = db.Column(db.Integer)
    constellation = db.Column(db.String(80), db.ForeignKey('constellations.latin_name'))
    planets = db.relationship('Planet', backref = 'stars')



    

#db.drop_all()
db.create_all()
