# -*- coding: utf-8 -*-
"""
Created on Sat Feb 10 11:26:29 2024
@author: olga

https://exoplanetarchive.ipac.caltech.edu/docs/API_PS_columns.html
V.1 - takes 3 systems (3 instances for each model connected by star (hostname))
"""

# json.dumps() — Takes in a Python object, and converts (dumps) it to a string.
# json.loads() — Takes a JSON string, and converts (loads) it to a Python object.

import requests, json
#response = requests.get("http://api.open-notify.org/astros.json")
# response = requests.get("https://exoplanetarchive.ipac.caltech.edu/TAP/sync?query=select+pl_name,hostname,pl_bmasse,pl_rade,pl_orbper,st_spectype,st_lum,st_mass,st_rad,st_dens,sy_dist,sy_snum,sy_pnum,sy_mnum+from+pscomppars+where+pl_name+=+'K2-3 b'+or+pl_name+=+'eps Eri b'+or+pl_name+=+'omi UMa b'+&format=json")
# #response = requests.get("https://exoplanetarchive.ipac.caltech.edu/TAP/sync?query=select+pl_name,hostname,pl_masse+from+ps+where+pl_name+=+'K2-3 b'+and+pl_masse+is+not+null+&format=json")
# print(response.status_code)
# print(response.json())

def jprint(obj):
    # create a formatted string of the Python JSON object
    text = json.dumps(obj, sort_keys=True, indent=4)
    print(text)
# jprint(response.json())


'''
####### For project 2 see a separate file 'project2_OEC_API.py'
###### OEC - Open Exoplanetary Catalog
##  https://github.com/OpenExoplanetCatalogue/open_exoplanet_catalogue/
## tree structure example
## https://www.openexoplanetcatalogue.com/planet/11%20Com%20b/
'''

# xml for project 2 parsing for Stars and Planets (info only here)
# =============================================================================
# ##copied text from https://github.com/OpenExoplanetCatalogue/open_exoplanet_catalogue/
# import xml.etree.ElementTree as ET, urllib.request, gzip, io
# url = "https://github.com/OpenExoplanetCatalogue/oec_gzip/raw/master/systems.xml.gz"
# # Decompress the gzipped content and Parse the decompressed content as XML
# oec = ET.parse(gzip.GzipFile(fileobj=io.BytesIO(urllib.request.urlopen(url).read())))
# =============================================================================


response = requests.get("https://gitlab.com/api/v4/projects/54362923/repository/commits?ref_name=main&per_page=100&author=ooneuro")
print(response.status_code)
print(len(response.json()))
jprint(response.json())

# https://gitlab.com/api/v4/projects/54362923/issues?per_page=100&author_username=ooneuro
## above link is an example for finding issues authored by a single author (all issues including closed and open)

