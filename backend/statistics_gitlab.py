"""
get stats on commits in repository in the form of a dictionary
for About page
"""
import requests

def count_commits():
    # get stats on commits in repository in the form of a list of dictionaries
    # will need to specify to get more than 20 results per page (per_page = 100)
    authors = ['Ela Albiston', 'ooneuro', 'Ryan Schedler', 'Monica Pham']
    usernames = ['elacanan', 'ooneuro', 'Mr.Pb', 'mownequa']

    response_body = {}

    for i, author in enumerate(authors):
        commits_response = requests.get("https://gitlab.com/api/v4/projects/54362923/repository/commits?ref_name=main&per_page=100&author=" + author).json()
        response_body[author + ' Commits'] = len(commits_response)
    
        issues_response = requests.get("https://gitlab.com/api/v4/projects/54362923/issues?per_page=100&author_username=" + usernames[i]).json()
        response_body[author + ' Issues'] = len(issues_response)

    response_body['totalCommits'] = len(requests.get("https://gitlab.com/api/v4/projects/54362923/repository/commits?ref_name=main&per_page=100").json())
    response_body['totalIssues'] = len(requests.get("https://gitlab.com/api/v4/projects/54362923/issues?per_page=100").json())

    return response_body