#!/usr/bin/env python3

# ---------------------------
# projects/IDB3/main.py
# WonderSpace
# ---------------------------

# -------
# imports
# -------

import os
import sys
import unittest
from models import db, Planet, Star, Constellation

# -----------
# DBTestCases
# -----------
class DBTestCases(unittest.TestCase):
    # ---------
    # stars
    # ---------
    
    def test_star_1(self):
        r = db.session.query(Star).filter_by(hostname = '11 Com').one()
        self.assertEqual(r.hostname, '11 Com')
        
    def test_star_2(self):
        #print(db.session.query(Star).filter_by(num_planets = 7).all())
        r = db.session.query(Star).filter_by(num_planets = 7).all()
        res = []
        for s in r:
            res.append(s.hostname)           
        #print(res)
        self.assertEqual(res, ['HD 219134', 'tau Ceti'])
        
    def test_star_3(self):
        r = db.session.query(Star).filter_by(magV = 6.11).filter_by(metallicity = 0.07).one()
        self.assertEqual(r.hostname, '38 Vir')
        
    def test_star_4(self):
        new_star = Star(hostname = 'Ela', magV = -100, constellation = 'Ursa Major') 
        db.session.add(new_star)
        db.session.commit()
        
        r = db.session.query(Star).filter_by(magV = -100).one()
        self.assertEqual(r.hostname, 'Ela')
        
        db.session.query(Star).filter_by(hostname = 'Ela').delete()
        db.session.commit()        
        
    # ---------
    # constellations
    # ---------
    
    def test_const_1 (self):       
        r = db.session.query(Constellation).filter_by(latin_name = 'Ursa Major').first()
        self.assertEqual(r.location, 'North Hemisphere Quadrant 2')   
    
    def test_const_2 (self):
        r = db.session.query(Constellation).filter_by(latin_name = 'Pisces').one()
        self.assertEqual(r.principal_star, 'Alpherg')
        
    def test_const_3 (self):
        new_constellar = Constellation(latin_name = 'Monica', eng_name = 'tester_constellation', declination = '-10\u00b0 10,10', principal_star = 'Olga', location = 'South Hemisphere Quadrant 4') 
        db.session.add(new_constellar)
        db.session.commit()
        
        r = db.session.query(Constellation).filter_by(latin_name = 'Monica').one()
        self.assertEqual(str(r.principal_star), 'Olga')
        
        db.session.query(Constellation).filter_by(latin_name = 'Monica').delete()
        db.session.commit()
        
    # ---------
    # planets
    # ---------
    def test_pl_1 (self):
        r = db.session.query(Planet).filter_by(plname = '47 UMa c').one()
        #self.assertEqual(r.location, 'North Hemisphere Quadrant 1')
        self.assertEqual(r.constellation, 'Ursa Major')
        
    def test_pl_2 (self):
        r = db.session.query(Planet).filter_by(mass = '8.82').filter_by(discoveryyear = 2002).first()
        #self.assertEqual(r.location, 'North Hemisphere Quadrant 1')
        self.assertEqual(r.plname, 'iota Draconis b')
        
    def test_pl_3 (self):
        new_planet = Planet(plname = 'Jerkyfox', hostname = '11 Com', constellation = 'Ursa Major') 
        db.session.add(new_planet)
        db.session.commit()
        
        r = db.session.query(Planet).filter_by(plname = 'Jerkyfox').one()
        self.assertEqual(r.plname, 'Jerkyfox')
        
        db.session.query(Planet).filter_by(plname = 'Jerkyfox').delete()
        db.session.commit()    

        

if __name__ == '__main__':
    unittest.main()
# end of code