// App.js
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Navbar from './components/Navbar';
import Home from './components/Home/Home';
import About from './components/About/About';
import UnitTests from './components/About/UnitTests';
import Planets from './components/Planets';
import Stars from './components/Stars';
import Constellations from './components/Constellations';
import PlanetDetail from './components/PlanetDetail';
import ConstellationDetail from './components/ConstellationDetail';
import StarDetail from './components/StarDetail';

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar />
        <div className="content">
          <Switch>
            <Route path="/home" component={Home} />
            <Route path="/about" render={(props) => (
            <About {...props} users={['Ela Albiston', 'ooneuro', 'Monica Pham', 'Ryan Schedler']} /> )} />
            <Route path="/planets/:plname" component={PlanetDetail} />            
            <Route path="/planets" component={Planets} />
            <Route path="/stars/:hostname" component={StarDetail} />
            <Route path="/stars" component={Stars} />
            <Route path="/constellations/:latin_name" component={ConstellationDetail} />
            <Route path="/constellations" component={Constellations} />
            <Route path="/unittests" component={UnitTests} />
            <Route path="*" component={Home} />
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
