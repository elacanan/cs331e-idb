import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Card, Col, Row, Container, Button} from 'react-bootstrap';
import ElaImage from '../../images/members/Headshot.JPG';
import OlgaImage from '../../images/members/Olga.jpg';
import MonicaImage from '../../images/members/Monica.JPG';
import RyanImage from '../../images/members/cat1.jpg';


const About = () => {
  const [data, setData] = useState({
    'Ela Albiston Commits': 0,
    'Ela Albiston Issues': 0,
    'ooneuro Commits': 0,
    'ooneuro Issues': 0,
    'Ryan Schedler Commits': 0,
    'Ryan Schedler Issues': 0,
    'Monica Pham Commits': 0,
    'Monica Pham Issues': 0,
    totalCommits: 0,
    totalIssues: 0,
  });
  const [loading, setLoading] = useState(true); 

  useEffect(() => {
    axios.get("https://backend-dot-crypto-volt-413616.uc.r.appspot.com/about")
      .then((response) => {
        setData(response.data);
        setLoading(false); 
      })
      .catch(err => {
        console.log(err);
        setLoading(false);
      });
  }, []);

if (!data) {
    return <div style={{color: "white"}}>Loading...</div>;
}

const TeamMembers = [
        {
          name: 'Ela Albiston',
          bio: 'Ela is a 4th year Neuroscience major at UT Austin pursuing the Elements of Computing certificate.',
          role: 'Project Leader and Frontend Developer',
          image: ElaImage, 
          commits: data['Ela Albiston Commits'],
          issues: data['Ela Albiston Issues'],
          unittests: 0
        },
        {
          name: 'Olga Ostrovskaya',
          bio: 'Olga is a neuroscientist and data junkie.',
          role: 'Backend Developer. Tools: APIs, databases.',
          image: OlgaImage,
          commits: data['ooneuro Commits'],
          issues: data['ooneuro Issues'],
          unittests: 9
        },
        {
            name: 'Monica Pham',
            bio: 'Monica is a super math senior at UT and completing computer science certificate.',
            role: 'Backend Developer',
            image: MonicaImage, 
            commits: data['Monica Pham Commits'],
            issues: data['Monica Pham Issues'],
            unittests: 1
        },
        {
            name: 'Ryan Schedler',
            bio: 'Ryan is a Behavioral Science major studying parental mental health and child attachment development.',
            role: 'Frontend + Flask application Developer',
            image: RyanImage, 
            commits: data['Ryan Schedler Commits'],
            issues: data['Ryan Schedler Issues'],
            unittests: 0
        }
    ]

    return (
        <Container fluid className="text-center" style={{ color: 'white' }}>
          <h1 className="my-5">About Us</h1>
          <h2 className="my-5">CS 331E Team 10</h2>
          <Row xs={1} md={4} className="g-4 justify-content-center">
            {TeamMembers.map((member, idx) => (
              <Col key={idx}>
                <Card>
                  <Card.Img variant="top" src={member.image} />
                  <Card.Body>
                    <Card.Title>{member.name}</Card.Title>
                    <Card.Text>{member.bio}</Card.Text>
                    <Card.Text><strong>Major Responsibilities: </strong> {member.role}</Card.Text>
                    <Card.Text>
                      <strong>Number of Commits: </strong> 
                      {loading ? "Loading statistics..." : member.commits}
                    </Card.Text>
                    <Card.Text>
                      <strong>Number of Issues: </strong> 
                      {loading ? "Loading statistics..." : member.issues}
                    </Card.Text>
                    <Card.Text>
                      <strong>Number of Unit Tests: </strong> 
                      {member.unittests}
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            ))}
          </Row>
          <br></br>
          <Row xs={1} md={4} className="g-4 justify-content-center">
            <Card style={{ width: '20rem'}}>
              <Card.Body>
                <Card.Title>Team Statistics</Card.Title>
                <Card.Text>Total Number of Commits: {loading ? "Loading..." : data.totalCommits}</Card.Text>
                <Card.Text>Total Number of Issues: {loading ? "Loading..." : data.totalIssues}</Card.Text>
                <Card.Text>Total Number of Unit Tests: 10</Card.Text>
                <Button variant="primary" href="/unittests">Run Unit Tests</Button>
              </Card.Body>
            </Card>
            
          
            <Card style={{ width: '20rem'}}>
              <Card.Body>
                <Card.Title>GitLab</Card.Title>
                <a href="https://gitlab.com/elacanan/cs331e-idb/-/issues">GitLab Issue Tracker</a>
                <br></br>
                <a href="https://gitlab.com/elacanan/cs331e-idb">GitLab Repo</a>
                <br></br>
                <a href="https://gitlab.com/elacanan/cs331e-idb/-/wikis/home">GitLab Wiki</a>
              </Card.Body>
            </Card>
          
            
            <Card style={{ width: '20rem'}}>
              <Card.Body>
                <Card.Title>Data</Card.Title>
                <Card.Text>Sources:</Card.Text>
                <a href="https://github.com/OpenExoplanetCatalogue/open_exoplanet_catalogue/">Open Exoplanet Catalogue</a>
                <br></br>
                <a href="https://www.datastro.eu/pages/home/">Datastro</a>
                <Card.Text>We scraped data for our Planet and Star models using an API 
                  from the Open Exoplanet Catalogue, and similarly obtained our Constellation model
                  data from an API provided by Datastro.</Card.Text>
              </Card.Body>
            </Card>

            <Card style={{ width: '20rem'}}>
              <Card.Body>
                <Card.Title>Tools</Card.Title>
                <Card.Text>Google Cloud Platform, Bootstrap, Flask, 
                  PostgreSQL, RESTful APIs, Dicsord, SQLAlchemy, git/GitLab,
                  unittest, coverage, GitLab CI, and pydoc.
                  </Card.Text>
                  <Card.Text>
                  <a href="https://documenter.getpostman.com/view/33692732/2sA3BheaJo">Postman API Documentation</a>
                  <br></br>
                  <a href="https://backend-dot-crypto-volt-413616.uc.r.appspot.com/constellations/api">Constellations API</a>
                  <br></br>
                  <a href="https://backend-dot-crypto-volt-413616.uc.r.appspot.com/stars/api">Stars API</a>
                  <br></br>
                  <a href="https://backend-dot-crypto-volt-413616.uc.r.appspot.com/planets/api">Planets API</a>
                  <br></br>
                  <a href="https://speakerdeck.com/mputexas/cs331e-idb-project-presentation">Presentation Slides</a>
	    	</Card.Text>
              </Card.Body>
            </Card>
            </Row>

            </Container>
          
      );
    };
    
    export default About;
    
