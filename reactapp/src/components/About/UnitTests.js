import React from 'react';
import Row from 'react-bootstrap/Row';
import TestImg from '../../images/unittests.png';
import Button from 'react-bootstrap/Button';
import Image from 'react-bootstrap/Image';

const UnitTests = () => {
    return (
        <Row className="my-5 justify-content-center">
            <Image src={TestImg}/>
            <Button href='/about/' style={{ width: '15rem'}}>Back to About</Button>
        </Row>
    )
}

export default UnitTests;