import React, { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';
import axios from 'axios';
import { Container, Row, Col, Card, ListGroup, Spinner } from 'react-bootstrap';
import styles from './Details.module.css';

const ConstellationDetail = () => {
  const { latin_name } = useParams();
  const [constellationDetails, setConstellationDetails] = useState(null);

  // extracts Latin name from URL to Fetch constellation details 
  useEffect(() => {
    axios.get(`https://backend-dot-crypto-volt-413616.uc.r.appspot.com/api/constellations/${latin_name}`)
    // axios.get(`http://127.0.0.1:8080/api/constellations/${latin_name}`)

      .then(res => {
        setConstellationDetails(res.data);
      })
      .catch(error => console.log(error));
  }, [latin_name]);

  // Data loading display
  if (!constellationDetails) {
    return (
      <Container className="text-center mt-5">
        <Spinner animation="border" role="status">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      </Container>
    );
  }

  return (
    
    
    <Container className="mt-3">
      {/* On screens Large or larger, display the cards in one row */}
      <Row className="justify-content-md-center">
        {/*  stars in the constellation card */}
        <Col md={2} className="d-none d-lg-block">
          <Card className={styles.detailCard}>
            <Card.Header className={styles.cardHeader}>
              Stars in this Constellation
            </Card.Header>
            <ListGroup variant="flush" className={styles.listCustom}>
              {constellationDetails.stars && constellationDetails.stars.length > 0 ? (
                constellationDetails.stars.map((star, index) => (
                  <ListGroup.Item key={index} className={styles.listItemCustom}>
                    <Link to={`/stars/${star.hostname}`} className={styles.starLink}>
                      {star.hostname}
                    </Link>
                  </ListGroup.Item>
                ))
              ) : (
                <ListGroup.Item className={styles.listItemCustom} style={{ textAlign: 'center' }}>No stars data available</ListGroup.Item>
              )}
            </ListGroup>
          </Card>
        </Col>
        {/* Main card with constellation attributes */}
        <Col lg={6} md={8}>
          <Card className={styles.detailCard}>
            <Card.Header as="h2" className={styles.cardHeader}>
              {constellationDetails.latin_name}
            </Card.Header>
            <Card.Body className={styles.mainCardBody}>
              <ListGroup variant="flush">
                <ListGroup.Item>
                  <span className={styles.attributeName}>English Name:</span> {constellationDetails.eng_name}
                </ListGroup.Item>
                <ListGroup.Item>
                  <span className={styles.attributeName}>Principal Star:</span> {constellationDetails.principal_star}
                </ListGroup.Item>
                <ListGroup.Item>
                  <span className={styles.attributeName}>Location:</span> {constellationDetails.location}
                </ListGroup.Item>
                <ListGroup.Item>
                  <span className={styles.attributeName}>Declination:</span> {constellationDetails.declination}
                </ListGroup.Item>
              </ListGroup>
              <Card.Img className={styles.cardImg} variant="bottom" src={constellationDetails.image_url} alt={constellationDetails.latin_name} />
            </Card.Body>
          </Card>
        </Col>
        {/*  planets found in the constellation system card */}
        <Col md={2} className="d-none d-lg-block">
          <Card className={styles.detailCard}>
            <Card.Header className={styles.cardHeader}>
              Planets in this Constellation
            </Card.Header>
            <ListGroup variant="flush" className={styles.listCustom}>
              {constellationDetails.planets && constellationDetails.planets.length > 0 ? (
                constellationDetails.planets.map((planet, index) => (
                  <ListGroup.Item key={index} className={styles.listItemCustom}>
                    <Link to={`/planets/${planet.plname}`} className={styles.planetLink}>
                      {planet.plname}
                    </Link>
                  </ListGroup.Item>
                ))
              ) : (
              <ListGroup.Item className={styles.listItemCustom} style={{ textAlign: 'center' }}>No planets data available</ListGroup.Item>
            )}
            </ListGroup>
          </Card>
        </Col>
      </Row>

      {/* On screens smaller than large, display the cards below the main card */}
      <Row className="justify-content-md-center mt-3 d-lg-none">
        <Col md={8}>
          <Card className={styles.detailCard}>
            <Card.Header className={styles.cardHeader}>
              Stars in this Constellation
            </Card.Header>
            <ListGroup variant="flush" horizontal className={styles.listCustom}>
                {constellationDetails.stars && constellationDetails.stars.length > 0 ? (
                  constellationDetails.stars.map((star, index) => (
                    <ListGroup.Item key={index} className={styles.listItemCustom}>
                      <Link to={`/stars/${star.hostname}`} className={styles.starLink}>
                        {star.hostname}
                      </Link>
                    </ListGroup.Item>
                  ))
                ) : (
            <ListGroup.Item style={{ textAlign: 'center' }}>No stars data available</ListGroup.Item>
          )}
            </ListGroup>
          </Card>
        </Col>
      </Row>
      <Row className="justify-content-md-center mt-3 d-lg-none">
        <Col md={8}>
          <Card className={styles.detailCard}>
            <Card.Header className={styles.cardHeader}>
              Planets in this Constellation
            </Card.Header>
            <ListGroup variant="flush" horizontal className={styles.listCustom}>
              {constellationDetails.planets && constellationDetails.planets.length > 0 ? (
                constellationDetails.planets.map((planet, index) => (
                  <ListGroup.Item key={index} className={styles.listItemCustom}>
                    <Link to={`/planets/${planet.plname}`} className={styles.planetLink}>
                      {planet.plname}
                    </Link>
                  </ListGroup.Item>
                ))
              ) : (
                <ListGroup.Item style={{ textAlign: 'center' }}>No planets data available</ListGroup.Item>
              )}
            </ListGroup>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default ConstellationDetail;