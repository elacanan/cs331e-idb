import React, { useState } from 'react';
import ModelDataFetcher from './ModelDataFetcher';
import ModelCard from './ModelCard';
import Pagination from './Pagination';
import { Container, Row, Col, InputGroup, FormControl, Dropdown, DropdownButton, Image } from 'react-bootstrap';
import ConstImg from '../images/systems/const.jpg';
import styles from './Model.module.css';

const Constellations = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [constellationsPerPage] = useState(16);
  const [searchTerm, setSearchTerm] = useState('');
  const [sort_by, setSortBy] = useState('eng_name');
  const [sort_order, setSortOrder] = useState('asc');

  const apiUrl = `https://backend-dot-crypto-volt-413616.uc.r.appspot.com/constellations/?query=${searchTerm}&sort_by=${sort_by}&sort_order=${sort_order}`;
  // const apiUrl = `http://127.0.0.1:8080/constellations/?query=${searchTerm}&sort_by=${sort_by}&sort_order=${sort_order}`;

  //  search input handler
  const handleSearchChange = (e) => {
    setSearchTerm(e.target.value);
    setCurrentPage(1); // Reset search page
  };

  // Updated to accept the direct value from Dropdown.Item
  const handleSortByChange = (eventKey) => {
    setSortBy(eventKey);
  };
  const handleSortOrderChange = (eventKey) => {
    setSortOrder(eventKey);
  };

  // Changes page 
  const paginate = pageNumber => setCurrentPage(pageNumber);

  return (
    <Container fluid>
      <Row className="justify-content-center py-4">
        <Col>
          <h1 className="display-4 text-center" style={{ color: 'white' }}>Constellations</h1>
        </Col>
      </Row>

      <Row className="justify-content-center py-3">
        <Col md={6}>
          <div className="d-flex align-items-center justify-content-center">
            <Image src={ConstImg} alt="Constellation" roundedCircle fluid/>
          </div>
        </Col>
      </Row>

      {/* const info */}
      <Row className="justify-content-center py-2">
        <Col md={8} className={styles.pageText}>
          <p className="text-light">Within the vast expanse of the Milky Way Galaxy, constellations serve as a celestial tapestry that has fascinated humanity throughout history. These configurations of stars, seen from Earth as distinctive patterns, have been imbued with mythologies, guiding explorers and inspiring poets and astronomers alike. The Milky Way, our galactic home, houses 88 officially recognized constellations, each with its unique assembly of stars and their surrounding planets.</p>
        </Col>
      </Row>
      
      {/* Search and sorting  */}
      <Row className="justify-content-center py-2">
        <Col xs={8} md={6} lg={3}>
          <InputGroup className="mb-3">
            <FormControl
              placeholder="Search constellations..."
              aria-label="Search constellations"
              value={searchTerm}
              onChange={handleSearchChange}
            />
          </InputGroup>
          <div className="text-center text-white">
            Search is case-sensitive.
          </div>
        </Col>
      </Row>

      {/*  Dropdowns  */}
      <Row className="justify-content-center">
        <Col xs={12} sm={6} md={4} lg={3} className="d-flex justify-content-center">
          <DropdownButton id="dropdown-sort-by" title="Sort By" variant="secondary" onSelect={handleSortByChange}>
            <Dropdown.Item eventKey="latin_name">Latin Name</Dropdown.Item>
            <Dropdown.Item eventKey="declination">Declination</Dropdown.Item>
            <Dropdown.Item eventKey="location">Location</Dropdown.Item>
            <Dropdown.Item eventKey="principal_star">Principal Star</Dropdown.Item>
          </DropdownButton>
          <DropdownButton id="dropdown-sort-order" title="Sort Order" variant="secondary" onSelect={handleSortOrderChange}>
            <Dropdown.Item eventKey="asc">Ascending</Dropdown.Item>
            <Dropdown.Item eventKey="desc">Descending</Dropdown.Item>
          </DropdownButton>
        </Col>
      </Row>

      {/* Constellation Cards  */}
      <ModelDataFetcher apiUrl={apiUrl}>
        {data => {
          const indexOfLastConstellation = currentPage * constellationsPerPage;
          const indexOfFirstConstellation = indexOfLastConstellation - constellationsPerPage;
          const currentConstellations = data.slice(indexOfFirstConstellation, indexOfLastConstellation);

          return (
            <>
              <Row>
                {currentConstellations.map((constellation, index) => (
                  <Col key={index} lg={3} md={4} className="mb-4">
                    <ModelCard
                      name={constellation.latin_name}
                      detailLink={`/constellations/${constellation.latin_name}`}
                      searchTerm={searchTerm} //   highlighting searchTerm
                    />
                  </Col>
                ))}
              </Row>
              <Row className="d-flex justify-content-center">
                <Pagination
                  itemsPerPage={constellationsPerPage}
                  totalItems={data.length}
                  paginate={paginate}
                  currentPage={currentPage}
                />
              </Row>
            </>
          );
        }}
      </ModelDataFetcher>
    </Container>
  );
};

export default Constellations;