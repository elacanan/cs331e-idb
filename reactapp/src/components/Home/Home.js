import React from 'react';
import { Col, Container, Carousel, Image, Row, Button } from 'react-bootstrap';
import Galaxy1 from '../../images/milkywaytimelapse.gif';
import StarTrail0 from '../../images/beginning-star-trail_16x9.jpg'
import StarTrail1 from '../../images/02-star-trail_16x9.jpg';
import StarTrail2 from '../../images/03-star-trail_16x9.jpg';
import StarTrail3 from '../../images/04-star-trail_16x9.jpg';
import StarTrail4 from '../../images/05-star-trail_16x9.jpg';
import Finally from '../../images/07finally-.jpeg';
import ConeNebula from '../../images/coneNebula.jpeg';
import HubbleNgc6956 from '../../images/hubble_ngc6956.jpg';
import MammothStars from '../../images/mammothStars.jpeg';
import PillarsOfCreation from '../../images/pillarsofCreation.jpeg';
import VeilNebula from '../../images/veilNebula.jpeg';
import MilkyWayCenter from '../../images/milkywayCenter.jpeg';
import './Home.css';




const Home = () => {
  return (
    <>
      <Container className="text-center my-4">
        <h1 className="display-4" style={{ color: 'white' }}>
          Welcome to the Milky Way.
        </h1>
        <span className="lead" style={{ color: 'white' }}>
          Join us on a journey through the cosmos and discover the beauty and mystery of our neighboring star systems.
        </span>
      </Container>

      <Container className="mb-4">
        <Row className="justify-content-center">
          <Col xs={12} md={4} className="text-center">
            <Button variant="light" href="/constellations/">Explore the MilkyWay</Button>
          </Col>
        </Row>
      </Container>

      <Carousel>
        <Carousel.Item>
                <Image src={Galaxy1} alt="Milky Way Time Lapse" className="d-block carousel-image frosted-glass" fluid />
            <Carousel.Caption className="custom-caption">
                <p>
                 March of the Milkyway timelapse, overlayed in 60mn intervals.
                </p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
                <Image src={StarTrail0} alt="Milky Way Time Lapse" className="d-block carousel-image frosted-glass" fluid />
            <Carousel.Caption className="custom-caption">
                <p>
                120 minute overlay
                </p>
            </Carousel.Caption>
        </Carousel.Item>

        <Carousel.Item>
          <Image src={StarTrail1} alt="Star Trail 1" className="d-block carousel-image frosted-glass" fluid  />
            <Carousel.Caption>
            <p>60 minute overlay</p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <Image src={StarTrail2} alt="Star Trail 2" className="d-block carousel-image frosted-glass" fluid  />
            <Carousel.Caption>
            <p>30 minute overlay</p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <Image src={StarTrail3} alt="Star Trail 3" className="d-block carousel-image frosted-glass" fluid  />
            <Carousel.Caption>
            <p>20 minute overlay</p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <Image src={StarTrail4} alt="Star Trail 4" className="d-block carousel-image frosted-glass" fluid  />
            <Carousel.Caption>
            <p>10 minute overlay</p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <Image src={Finally} alt="Finally" className="d-block carousel-image frosted-glass" fluid  />
            <Carousel.Caption>
            <p>Long Exposure shot of an entire night </p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <Image src={ConeNebula} alt="ConeNebula" className="d-block carousel-image frosted-glass" roundedCircle fluid  />
            <Carousel.Caption>
            <p>The Cone Nebula, a pillar of star formation</p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <Image src={HubbleNgc6956} alt="HubbleNgc6956" className="d-block carousel-image frosted-glass" roundedCircle fluid  />
            <Carousel.Caption>
            <p>Depiction of the Milkway's Spiral formation</p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <Image src={MammothStars} alt="MammothStars2" className="d-block carousel-image frosted-glass" roundedCircle fluid  />
            <Carousel.Caption>
            <p>The birthplace of mammoth stars</p>
            </Carousel.Caption>
        </Carousel.Item>
     <Carousel.Item>
          <Image src={PillarsOfCreation} alt="PillarsOfCreation" className="d-block carousel-image frosted-glass" roundedCircle fluid  />
            <Carousel.Caption>
            <p>The iconic Pillars of Creation within the Eagle Nebula</p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <Image src={VeilNebula} alt="VeilNebula" className="d-block carousel-image frosted-glass" roundedCircle fluid  />
            <Carousel.Caption>
            <p>Remnants of a supernova explosion in the form of the Veil Nebula</p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <Image src={MilkyWayCenter} alt="MilkyWayCenter" className="d-block carousel-image frosted-glass" roundedCircle fluid  />
            <Carousel.Caption>
            <p>The galactic center of the Milky Way, teeming with stars</p>
            </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </>
  );
};

export default Home;