import React from 'react';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import styles from './Model.module.css';

// Highlight function
const highlightText = (text, searchTerm) => {
  if (!searchTerm.trim()) return text; // If no search term, return text

  const regex = new RegExp(`(${searchTerm})`, 'gi');
  const highlightedText = text.replace(regex, match => `<mark class="highlight">${match}</mark>`);
  return highlightedText;
};

// ModelCard component 
const ModelCard = ({ name, detailLink, searchTerm }) => {
  const highlightedName = highlightText(name, searchTerm);

  return (
    <Link to={detailLink} className={styles.cardLink}>
      <Card className={`${styles.card} mx-auto my-2`}>
        <Card.Body className="text-center">
          <Card.Title dangerouslySetInnerHTML={{ __html: highlightedName }} className={styles.cardTitle}></Card.Title>
        </Card.Body>
      </Card>
    </Link>
  );
};

export default ModelCard;