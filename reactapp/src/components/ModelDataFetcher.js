import React, { useState, useEffect } from 'react';
import axios from 'axios';

// component handles data fetching for all 3 models and handles loading and error states.
const ModelDataFetcher = ({ apiUrl, children }) => {
  const [data, setData] = useState([]);
  // State to track whether data is still loading
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    axios.get(apiUrl)
      .then(res => {
        // Set fetched data to state
        setData(res.data);
        setIsLoading(false);
      })
      .catch(error => {
        // If an error occurs, sets  error state and indicate loading is complete
        setError(error);
        setIsLoading(false);
      });
  }, [apiUrl]);  //  apiUrl dependency

  return (
    <>
      {error && <div>{error.message}</div>}  
      {isLoading ? <div>Loading...</div> : children(data)} 
    </>
  );
};

export default ModelDataFetcher;