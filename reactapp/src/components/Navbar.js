import React from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import 'bootstrap/dist/css/bootstrap.min.css';
import styles from './Details.module.css'; 


const NavBar = () => {
  return (
    <Navbar expand="lg" className={`${styles.navBar} bg-dark`} variant="dark">
      <Container>
        <Navbar.Brand href="/home" style={{ color: 'white' }}>
          SpaceWanderer
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/planets/" className={styles.navLink}>Planets</Nav.Link>
            <Nav.Link href="/stars/" className={styles.navLink}>Stars</Nav.Link>
            <Nav.Link href="/constellations/" className={styles.navLink}>Constellations</Nav.Link>
            <Nav.Link href="/about/" className={styles.navLink}>About</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default NavBar;
