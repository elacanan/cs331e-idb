import React, { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';
import axios from 'axios';
import { Container, Row, Col, Card, ListGroup, Spinner } from 'react-bootstrap';
import styles from './Details.module.css';

const PlanetDetail = () => {
  const { plname } = useParams(); 
  const [planetDetails, setPlanetDetails] = useState(null); // storing planet details

  // Fetches planet details on component mount or when plname changes
  useEffect(() => {
    axios.get(`https://backend-dot-crypto-volt-413616.uc.r.appspot.com/api/planets/${plname}`)
    //axios.get(`http://127.0.0.1:8080/api/planets/${plname}`)
      .then(res => {
        setPlanetDetails(res.data);  // Stores planet details in state
      })
      .catch(error => console.log(error)); 
  }, [plname]);

  // loading display 
  if (!planetDetails) {
    return (
      <Container className="text-center mt-5">
        <Spinner animation="border" role="status">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      </Container>
    );
  }

  return (
    <Container className="mt-3">
      <Row className="justify-content-md-center">
        <Col md={6}>
          {/* basic planet details card */}
          <Card className={styles.detailCard}>
            <Card.Header as="h2" className={styles.cardHeader}>
              {planetDetails.plname}
            </Card.Header>
            <Card.Body className={styles.mainCardBody}>
                {/* listgroup of planet attributes */}
              <ListGroup variant="flush">
                <ListGroup.Item>
                  <span className={styles.attributeName}>Status:</span> {planetDetails.status}
                </ListGroup.Item>
                <ListGroup.Item>
                  <span className={styles.attributeName}>Mass:</span> {planetDetails.mass} (in Jupiter masses)
                </ListGroup.Item>
                <ListGroup.Item>
                  <span className={styles.attributeName}>Orbital Period:</span> {planetDetails.period} days
                </ListGroup.Item>
                <ListGroup.Item>
                  <span className={styles.attributeName}>Semi-major Axis:</span> {planetDetails.semimajoraxis} AU
                </ListGroup.Item>
                <ListGroup.Item>
                  <span className={styles.attributeName}>Eccentricity:</span> {planetDetails.eccentricity}
                </ListGroup.Item>
                <ListGroup.Item>
                  <span className={styles.attributeName}>Periastron:</span> {planetDetails.periastron} degrees
                </ListGroup.Item>
                <ListGroup.Item>
                  {/* Links to the host stars detailpage */}
                  <span className={styles.attributeName}>Host Star:</span>
                  <Link to={`/stars/${planetDetails.hostname}`} className={styles.planetLink}>
                    {planetDetails.hostname}
                  </Link>
                </ListGroup.Item>
                <ListGroup.Item>
                  {/* Links to the related const's detail page  */}
                  <span className={styles.attributeName}>Constellation:</span>
                  <Link to={`/constellations/${planetDetails.constellation}`} className={styles.planetLink}>
                    {planetDetails.constellation}
                  </Link>
                </ListGroup.Item>
              </ListGroup>
            </Card.Body>
          </Card>
        </Col>

        {/* Conditionally renders if description is available */}
        {planetDetails.description && (
          <Col md={6}>
            <Card className={styles.detailCard}>
              <Card.Header className={styles.cardHeader}>Discovery Year: {planetDetails.discoveryyear}</Card.Header>
              <Card.Body className={styles.mainCardBody}>
                <Card.Text>
                  {planetDetails.description}
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        )}
      </Row>
    </Container>
  );
};

export default PlanetDetail;