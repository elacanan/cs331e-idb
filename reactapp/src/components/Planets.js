import React, { useState } from 'react';
import { Container, Row, Col, InputGroup, FormControl, Dropdown, DropdownButton, Image } from 'react-bootstrap';
import ModelDataFetcher from './ModelDataFetcher';
import ModelCard from './ModelCard';
import Pagination from './Pagination';
import PlanetImg from '../images/planets/prettyplanet.jpg';
import styles from './Model.module.css';

const Planets = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [planetsPerPage] = useState(16);
  const [searchTerm, setSearchTerm] = useState('');
  const [sort_by, setSortBy] = useState('mass');
  const [sort_order, setSortOrder] = useState('asc');

  const apiUrl = `https://backend-dot-crypto-volt-413616.uc.r.appspot.com/planets/?query=${searchTerm}&sort_by=${sort_by}&sort_order=${sort_order}`;
  //const apiUrl = `http://127.0.0.1:8080/planets/?query=${searchTerm}&sort_by=${sort_by}&sort_order=${sort_order}`;

  // Handles updates to the search input field
  const handleSearchChange = (e) => {
    setSearchTerm(e.target.value);
    setCurrentPage(1); // Reset to first page after  new search
  };

  // Updated to accept the direct value from Dropdown.Item
  const handleSortByChange = (value) => {
    setSortBy(value);
  };
  
  const handleSortOrderChange = (value) => {
    setSortOrder(value);
  };

  // Changes the page number
  const paginate = pageNumber => setCurrentPage(pageNumber);

  return (
    <Container fluid>
      <Row className="justify-content-center py-4">
        <Col>
          <h1 className="display-4 text-center" style={{ color: 'white' }}>Planets</h1>
        </Col>
      </Row>

      <Row className="justify-content-center py-3">
        <Col md={6}>
          <div className="d-flex align-items-center justify-content-center">
            <Image src={PlanetImg} alt="Planets" roundedCircle fluid/>
          </div>
        </Col>
      </Row>

      {/* planet info */}
      <Row className="justify-content-center py-2">
        <Col md={8} className={styles.pageText}>
          <p className="text-light">Exoplanets, or planets outside our solar system, populate the Milky Way in astonishing diversity and abundance. These celestial bodies orbit stars other than our Sun, and their discovery has revolutionized our understanding of planetary systems. Ranging from gas giants larger than Jupiter to rocky worlds smaller than Earth, exoplanets exhibit a wide array of characteristics, compositions, and orbits, challenging our preconceived notions of what planets can be like.</p>
        </Col>
      </Row>
      
      {/* Search and Sort Row */}
      <Row className="justify-content-center py-2">
        <Col xs={8} md={6} lg={3}>
          <InputGroup className="mb-3">
            <FormControl
              placeholder="Search planets..."
              aria-label="Search planets"
              value={searchTerm}
              onChange={handleSearchChange}
            />
          </InputGroup>
          <div className="text-center text-white">
            Search is case-sensitive.
          </div>
        </Col>
      </Row>

      {/* Sorting Dropdowns Row */}
      <Row className="justify-content-center">
        <Col xs={12} sm={6} md={4} lg={3} className="d-flex justify-content-center">
          <DropdownButton id="dropdown-sort-by" title="Sort By" variant="secondary" onSelect={handleSortByChange}>
            <Dropdown.Item eventKey="mass">Mass</Dropdown.Item>
            <Dropdown.Item eventKey="period">Orbital Period</Dropdown.Item>
            <Dropdown.Item eventKey="semimajoraxis">Semi-major Axis</Dropdown.Item>
            <Dropdown.Item eventKey="eccentricity">Eccentricity</Dropdown.Item>
            <Dropdown.Item eventKey="discoveryyear">Discovery Year</Dropdown.Item>
          </DropdownButton>
          <DropdownButton id="dropdown-sort-order" title="Sort Order" variant="secondary" onSelect={handleSortOrderChange}>
            <Dropdown.Item eventKey="asc">Ascending</Dropdown.Item>
            <Dropdown.Item eventKey="desc">Descending</Dropdown.Item>
          </DropdownButton>
        </Col>
      </Row>

      {/* Planet Cards */}
      <ModelDataFetcher apiUrl={apiUrl}>
        {data => {
          const indexOfLastPlanet = currentPage * planetsPerPage;
          const indexOfFirstPlanet = indexOfLastPlanet - planetsPerPage;
          const currentPlanets = data.slice(indexOfFirstPlanet, indexOfLastPlanet);

          return (
            <>
              <Row>
                {currentPlanets.map((planet, index) => (
                  <Col key={index} lg={3} md={4} className="mb-4">
                    <ModelCard
                      name={planet.plname}
                      detailLink={`/planets/${planet.plname}`}
                      searchTerm={searchTerm} // Pass highlighting search term
                    />
                  </Col>
                ))}
              </Row>
              <Row className="d-flex justify-content-center">
                <Pagination
                  itemsPerPage={planetsPerPage}
                  totalItems={data.length}
                  paginate={paginate}
                  currentPage={currentPage}
                />
              </Row>
            </>
          );
        }}
      </ModelDataFetcher>
    </Container>
  );
};

export default Planets;