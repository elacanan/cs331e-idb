import React, { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';
import axios from 'axios';
import { Container, Row, Col, Card, ListGroup, Spinner } from 'react-bootstrap';
import styles from './Details.module.css';

const StarDetail = () => {
  const { hostname } = useParams(); // Extract hostname from URL parameters
  const [starDetails, setStarDetails] = useState(null); // State to hold star details

  // Fetch star details
  useEffect(() => {
    axios.get(`https://backend-dot-crypto-volt-413616.uc.r.appspot.com/api/stars/${hostname}`)
    //axios.get(`http://127.0.0.1:8080/api/stars/${hostname}`)
      .then(res => {
        setStarDetails(res.data);
      })
      .catch(error => console.log(error));
  }, [hostname]);

  // loading display
  if (!starDetails) {
    return (
      <Container className="text-center mt-5">
        <Spinner animation="border" role="status">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      </Container>
    );
  }

  return (
    <Container className="mt-3">
      {/*  star details / attribute data */}
      <Row className="justify-content-md-center">
        <Col lg={6}>
          <Card className={styles.detailCard}>
            <Card.Header as="h2" className={styles.cardHeader}>
              {starDetails.hostname}
            </Card.Header>
            <Card.Body className={styles.mainCardBody}>
              <ListGroup variant="flush">
                <ListGroup.Item>
                  <span className={styles.attributeName}>Spectral Type:</span> {starDetails.spectraltype}
                </ListGroup.Item>
                <ListGroup.Item>
                  <span className={styles.attributeName}>Mass:</span> {starDetails.mass} (in solar masses)
                </ListGroup.Item>
                <ListGroup.Item>
                  <span className={styles.attributeName}>Mag V:</span> {starDetails.magV}
                </ListGroup.Item>
                <ListGroup.Item>
                  <span className={styles.attributeName}>Radius:</span> {starDetails.radius} (in solar radii)
                </ListGroup.Item>
                <ListGroup.Item>
                  <span className={styles.attributeName}>Metallicity:</span> {starDetails.metallicity}
                </ListGroup.Item>
                <ListGroup.Item>
                  <span className={styles.attributeName}>Constellation:</span>
                  <Link to={`/constellations/${starDetails.constellation}`} className={styles.planetLink}>
                    {starDetails.constellation}
                  </Link>
                </ListGroup.Item>
              </ListGroup>
            </Card.Body>
          </Card>
        </Col>

        {/* Display for lg screens or larger */}
        <Col lg={4} className="d-none d-lg-block">
          <Card className={`${styles.detailCard} ${styles.planetListCard}`}>
            <Card.Header className={styles.cardHeader}>
              {starDetails.num_planets} Planets in this Star System
            </Card.Header>
            <Card.Body className={styles.mainCardBody}>
              {starDetails.planets && starDetails.planets.length > 0 ? (
                <ListGroup variant="flush" className={styles.listCustom}>
                  {starDetails.planets.map((planet, index) => (
                    <ListGroup.Item key={index} className={styles.listItemCustom}>
                      <Link to={`/planets/${planet.plname}`} className={styles.planetLink}>{planet.plname}</Link>
                    </ListGroup.Item>
                  ))}
                </ListGroup>
              ) : (
                <Card.Text>No planets associated with this star.</Card.Text>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>

      {/* Display for screens smaller than lg */}
      <Row className="justify-content-md-center mt-3 d-lg-none">
        <Col md={6}>
          <Card className={`${styles.detailCard} ${styles.planetListCard}`}>
            <Card.Header className={styles.cardHeader}>
              {starDetails.num_planets} Planets in this Star System
            </Card.Header>
            <Card.Body className={styles.mainCardBody}>
              {starDetails.planets && starDetails.planets.length > 0 ? (
                <ListGroup variant="flush" horizontal className={styles.listCustom}>
                  {starDetails.planets.map((planet, index) => (
                    <ListGroup.Item key={index} className={styles.listItemCustom}>
                      <Link to={`/planets/${planet.plname}`} className={styles.planetLink}>{planet.plname}</Link>
                    </ListGroup.Item>
                  ))}
                </ListGroup>
              ) : (
                <Card.Text>No planets associated with this star.</Card.Text>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default StarDetail;