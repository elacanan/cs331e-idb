import React, { useState } from 'react';
import { Container, Row, Col, FormControl, DropdownButton, Dropdown, InputGroup, Image } from 'react-bootstrap';
import ModelDataFetcher from './ModelDataFetcher';
import ModelCard from './ModelCard';
import Pagination from './Pagination';
import StarImg from '../images/stars/prettystar.jpg';
import styles from './Model.module.css';

const Stars = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [starsPerPage] = useState(16);
  const [searchTerm, setSearchTerm] = useState('');
  const [sort_by, setSortBy] = useState('mass');
  const [sort_order, setSortOrder] = useState('asc');

  const apiUrl = `https://backend-dot-crypto-volt-413616.uc.r.appspot.com/stars/?query=${searchTerm}&sort_by=${sort_by}&sort_order=${sort_order}`;
  //const apiUrl = `http://127.0.0.1:8080/stars/?query=${searchTerm}&sort_by=${sort_by}&sort_order=${sort_order}`;

  const handleSearchChange = (e) => {
    setSearchTerm(e.target.value);
    setCurrentPage(1); // Reset to the first page after new search
  };

  const handleSortByChange = (eventKey) => {
    setSortBy(eventKey);
  };

  const handleSortOrderChange = (eventKey) => {
    setSortOrder(eventKey);
  };

  const paginate = pageNumber => setCurrentPage(pageNumber);

  return (
    <Container fluid>
      <Row className="justify-content-center py-4">
        <Col>
          <h1 className="display-4 text-center" style={{ color: 'white' }}>Stars</h1>
        </Col>
      </Row>

      {/* Star image display */}
      <Row className="justify-content-center py-3">
        <Col md={6}>
          <div className="d-flex align-items-center justify-content-center">
            <Image src={StarImg} alt="Star" roundedCircle fluid />
          </div>
        </Col>
      </Row>

      {/*  stars info */}
      <Row className="justify-content-center py-2">
        <Col md={8} className={styles.pageText}>
          <p className="text-light">Stars in the Milky Way form a vast array that spans our galaxy's sprawling disk, bulge, and halo. This grand stellar assembly, numbering in the billions, encompasses a diverse range of types, sizes, and luminosities. The Milky Way's stars are not evenly distributed; instead, they are primarily clustered within its spiral arms—great lanes of dust, gas, and stars that coil around the galaxy's luminous core. </p>
        </Col>
      </Row>

      {/* Search and sorting  */}
      <Row className="justify-content-center py-2">
        <Col xs={8} md={6} lg={3}>
          <InputGroup className="mb-3">
            <FormControl
              placeholder="Search stars..."
              aria-label="Search stars"
              value={searchTerm}
              onChange={handleSearchChange}
            />
          </InputGroup>
          <div className="text-center text-white">
            Search is case-sensitive.
          </div>
        </Col>
      </Row>

      {/* Dropdown for sorting options */}
      <Row className="justify-content-center">
        <Col xs={12} sm={6} md={4} lg={3} className="d-flex justify-content-center">
          <DropdownButton id="dropdown-sort-by" title="Sort By" variant="secondary" onSelect={handleSortByChange}>
            <Dropdown.Item eventKey="magV">Magnitude</Dropdown.Item>
            <Dropdown.Item eventKey="mass">Mass</Dropdown.Item>
            <Dropdown.Item eventKey="radius">Radius</Dropdown.Item>
            <Dropdown.Item eventKey="metallicity">Metallicity</Dropdown.Item>
            <Dropdown.Item eventKey="num_planets">Number of Planets</Dropdown.Item>
          </DropdownButton>
          <DropdownButton id="dropdown-sort-order" title="Sort Order" variant="secondary" onSelect={handleSortOrderChange}>
            <Dropdown.Item eventKey="asc">Ascending</Dropdown.Item>
            <Dropdown.Item eventKey="desc">Descending</Dropdown.Item>
          </DropdownButton>
        </Col>
      </Row>

      {/* Data fetching and rendering of stars */}
      <ModelDataFetcher apiUrl={apiUrl}>
        {data => {
          const indexOfLastStar = currentPage * starsPerPage;
          const indexOfFirstStar = indexOfLastStar - starsPerPage;
          const currentStars = data.slice(indexOfFirstStar, indexOfLastStar);

          return (
            <>
              <Row>
                {currentStars.map((star, index) => (
                  <Col key={index} lg={3} md={4} className="mb-4">
                    <ModelCard
                      name={star.hostname}
                      detailLink={`/stars/${star.hostname}`}
                      searchTerm={searchTerm} // added searchTerm to the ModelCard for highlighting
                    />
                  </Col>
                ))}
              </Row>
              <Row className="d-flex justify-content-center">
                <Pagination
                  itemsPerPage={starsPerPage}
                  totalItems={data.length}
                  paginate={paginate}
                  currentPage={currentPage}
                />
              </Row>
            </>
          );
        }}
      </ModelDataFetcher>
    </Container>
  );
};

export default Stars;